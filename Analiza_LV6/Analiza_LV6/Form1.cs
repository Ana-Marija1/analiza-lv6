﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Analiza_LV6
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public static string proslaJednadzba = "", proslaOperacija = "", operacija = "";

        public static double rjesenje = 0;

        public static int zastavica = 0;

        private void bTocka_Click(object sender, EventArgs e)
        {
            if (!txtPrikaz1.Text.Contains("."))
            {
                txtPrikaz1.Text += ".";
            }
        }

        private void bNula_Click(object sender, EventArgs e)
        {
            txtPrikaz1.Text += "0";
        }

        private void bJednako_Click(object sender, EventArgs e)
        {
            if (txtPrikaz1.Text.Length > 0)
            {
                operacija = "=";
                viseJednadzbi();
                txtPrikaz2.ResetText();
                txtPrikaz1.Text = rjesenje.ToString();
            }
        }

        private void bPlus_Click(object sender, EventArgs e)
        {
            if (txtPrikaz1.Text.Length > 0)
            {
                if(operacija == "" || operacija == "=")
                {
                    operacija = "+";
                    proslaOperacija = operacija;
                    proslaJednadzba = txtPrikaz1.Text;
                    txtPrikaz2.Text = proslaJednadzba + operacija;
                    txtPrikaz1.ResetText();
                }
                else
                {
                    operacija = "+";
                    viseJednadzbi();
                }
            }
        }

        private void bJedan_Click(object sender, EventArgs e)
        {
            txtPrikaz1.Text += "1";
        }

        private void bDva_Click(object sender, EventArgs e)
        {
            txtPrikaz1.Text += "2";
        }

        private void bTri_Click(object sender, EventArgs e)
        {
            txtPrikaz1.Text += "3";
        }

        private void bMinus_Click(object sender, EventArgs e)
        {
            if (txtPrikaz1.Text.Length > 0)
            {
                if (operacija == "" || operacija == "=")
                {
                    operacija = "-";
                    proslaOperacija = operacija;
                    proslaJednadzba = txtPrikaz1.Text;
                    txtPrikaz2.Text = proslaJednadzba + operacija;
                    txtPrikaz1.ResetText();
                }
                else
                {
                    operacija = "-";
                    viseJednadzbi();
                }
            }
        }

        private void bCetiri_Click(object sender, EventArgs e)
        {
            txtPrikaz1.Text += "4";
        }

        private void bPet_Click(object sender, EventArgs e)
        {
            txtPrikaz1.Text += "5";
        }

        private void bSest_Click(object sender, EventArgs e)
        {
            txtPrikaz1.Text += "6";
        }

        private void bPuta_Click(object sender, EventArgs e)
        {
            if (txtPrikaz1.Text.Length > 0)
            {
                if (operacija == "" || operacija == "=")
                {
                    operacija = "x";
                    proslaOperacija = operacija;
                    proslaJednadzba = txtPrikaz1.Text;
                    txtPrikaz2.Text = proslaJednadzba + operacija;
                    txtPrikaz1.ResetText();
                }
                else
                {
                    operacija = "x";
                    viseJednadzbi();
                }
            }
        }

        private void bSedam_Click(object sender, EventArgs e)
        {
            txtPrikaz1.Text += "7";
        }

        private void bOsam_Click(object sender, EventArgs e)
        {
            txtPrikaz1.Text += "8";
        }

        private void bDevet_Click(object sender, EventArgs e)
        {
            txtPrikaz1.Text += "9";
        }

        private void bPodijeljeno_Click(object sender, EventArgs e)
        {
            if (txtPrikaz1.Text.Length > 0)
            {
                if (operacija == "" || operacija == "=")
                {
                    operacija = "/";
                    proslaOperacija = operacija;
                    proslaJednadzba = txtPrikaz1.Text;
                    txtPrikaz2.Text = proslaJednadzba + operacija;
                    txtPrikaz1.ResetText();
                }
                else
                {
                    operacija = "/";
                    viseJednadzbi();
                }
            }
        }

        private void bKosinus_Click(object sender, EventArgs e)
        {
            if (txtPrikaz1.Text.Length > 0)
            {
                if (operacija == "" || operacija == "=")
                {
                    operacija = "cos";
                    proslaOperacija = operacija;
                    proslaJednadzba = txtPrikaz1.Text;
                    rjesenje = Math.Cos(Convert.ToDouble(proslaJednadzba));
                    txtPrikaz2.Text = operacija + "(" + proslaJednadzba + ")";
                    proslaJednadzba = rjesenje.ToString();
                    txtPrikaz1.Text = proslaJednadzba;
                }
            }
        }

        private void bKorijen_Click(object sender, EventArgs e)
        {
            if (txtPrikaz1.Text.Length > 0)
            {
                if (operacija == "" || operacija == "=")
                {
                    operacija = "sqrt";
                    proslaOperacija = operacija;
                    proslaJednadzba = txtPrikaz1.Text;
                    rjesenje = Math.Sqrt(Convert.ToDouble(proslaJednadzba));
                    txtPrikaz2.Text = operacija + "(" + proslaJednadzba + ")";
                    proslaJednadzba = rjesenje.ToString();
                    txtPrikaz1.Text = proslaJednadzba;
                }
            }
        }

        private void bKvadrat_Click(object sender, EventArgs e)
        {
            if (txtPrikaz1.Text.Length > 0)
            {
                if (operacija == "" || operacija == "=")
                {
                    operacija = "pow";
                    proslaOperacija = operacija;
                    proslaJednadzba = txtPrikaz1.Text;
                    rjesenje = Math.Pow(Convert.ToDouble(proslaJednadzba),2);
                    txtPrikaz2.Text = operacija + "(" + proslaJednadzba + ")";
                    proslaJednadzba = rjesenje.ToString();
                    txtPrikaz1.Text = proslaJednadzba;
                }
            }
        }
        private void bLogaritam_Click(object sender, EventArgs e)
        {
            if (txtPrikaz1.Text.Length > 0)
            {
                if (operacija == "" || operacija == "=")
                {
                    operacija = "log";
                    proslaOperacija = operacija;
                    proslaJednadzba = txtPrikaz1.Text;
                    rjesenje = Math.Log10(Convert.ToDouble(proslaJednadzba));
                    txtPrikaz2.Text = operacija + "(" + proslaJednadzba + ")";
                    proslaJednadzba = rjesenje.ToString();
                    txtPrikaz1.Text = proslaJednadzba;
                }
            }
        }

        private void bSinus_Click(object sender, EventArgs e)
        {
            if (txtPrikaz1.Text.Length > 0)
            {
                if (operacija == "" || operacija == "=")
                {
                    operacija = "sin";
                    proslaOperacija = operacija;
                    proslaJednadzba = txtPrikaz1.Text;
                    rjesenje = Math.Sin(Convert.ToDouble(proslaJednadzba));
                    txtPrikaz2.Text = operacija + "(" + proslaJednadzba + ")";
                    proslaJednadzba = rjesenje.ToString();
                    txtPrikaz1.Text = proslaJednadzba;
                }
            }
        }

        private void bObrisiDio_Click(object sender, EventArgs e)
        {
            txtPrikaz1.ResetText();
        }
        private void bObrisiSve_Click(object sender, EventArgs e)
        {
            operacija = "";
            txtPrikaz1.ResetText();
            txtPrikaz2.ResetText();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void bDel_Click(object sender, EventArgs e)
        {
            if (txtPrikaz1.Text.Length > 0)
            {
                txtPrikaz1.Text = txtPrikaz1.Text.Substring(0, txtPrikaz1.Text.Length - 1);
            }
        }
         private void viseJednadzbi()
        {
            if (proslaOperacija == "+")
            {
                proslaOperacija = operacija;
                rjesenje = Convert.ToDouble(proslaJednadzba) + Convert.ToDouble(txtPrikaz1.Text);
                proslaJednadzba = rjesenje.ToString();
                txtPrikaz2.Text = proslaJednadzba + operacija;
                txtPrikaz1.ResetText();
            }
            else if (proslaOperacija == "-")
            {
                proslaOperacija = operacija;
                rjesenje = Convert.ToDouble(proslaJednadzba) - Convert.ToDouble(txtPrikaz1.Text);
                proslaJednadzba = rjesenje.ToString();
                txtPrikaz2.Text = proslaJednadzba + operacija;
                txtPrikaz1.ResetText();
            }
            else if (proslaOperacija == "x")
            {
                proslaOperacija = operacija;
                rjesenje = Convert.ToDouble(proslaJednadzba) * Convert.ToDouble(txtPrikaz1.Text);
                proslaJednadzba = rjesenje.ToString();
                txtPrikaz2.Text = proslaJednadzba + operacija;
                txtPrikaz1.ResetText();
            }
            else if (proslaOperacija == "/")
            {
                proslaOperacija = operacija;
                rjesenje = Convert.ToDouble(proslaJednadzba) / Convert.ToDouble(txtPrikaz1.Text);
                proslaJednadzba = rjesenje.ToString();
                txtPrikaz2.Text = proslaJednadzba + operacija;
                txtPrikaz1.ResetText();
            }
            else if (proslaOperacija == "cos")
            {
                
            }
        }
    }
}
