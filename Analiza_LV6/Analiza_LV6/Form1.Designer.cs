﻿namespace Analiza_LV6
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.bLogaritam = new System.Windows.Forms.Button();
            this.bNula = new System.Windows.Forms.Button();
            this.bTocka = new System.Windows.Forms.Button();
            this.bJednako = new System.Windows.Forms.Button();
            this.bJedan = new System.Windows.Forms.Button();
            this.bDva = new System.Windows.Forms.Button();
            this.bTri = new System.Windows.Forms.Button();
            this.bPlus = new System.Windows.Forms.Button();
            this.bCetiri = new System.Windows.Forms.Button();
            this.bPet = new System.Windows.Forms.Button();
            this.bSest = new System.Windows.Forms.Button();
            this.bMinus = new System.Windows.Forms.Button();
            this.bSedam = new System.Windows.Forms.Button();
            this.bOsam = new System.Windows.Forms.Button();
            this.bDevet = new System.Windows.Forms.Button();
            this.bPuta = new System.Windows.Forms.Button();
            this.bKosinus = new System.Windows.Forms.Button();
            this.bKorijen = new System.Windows.Forms.Button();
            this.bKvadrat = new System.Windows.Forms.Button();
            this.bPodijeljeno = new System.Windows.Forms.Button();
            this.bSinus = new System.Windows.Forms.Button();
            this.bObrisiDio = new System.Windows.Forms.Button();
            this.bObrisiSve = new System.Windows.Forms.Button();
            this.bDel = new System.Windows.Forms.Button();
            this.txtPrikaz2 = new System.Windows.Forms.TextBox();
            this.txtPrikaz1 = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // bLogaritam
            // 
            this.bLogaritam.BackColor = System.Drawing.Color.LightSteelBlue;
            this.bLogaritam.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.bLogaritam.Location = new System.Drawing.Point(207, 156);
            this.bLogaritam.Name = "bLogaritam";
            this.bLogaritam.Size = new System.Drawing.Size(59, 56);
            this.bLogaritam.TabIndex = 0;
            this.bLogaritam.Text = "log";
            this.bLogaritam.UseVisualStyleBackColor = false;
            this.bLogaritam.Click += new System.EventHandler(this.bLogaritam_Click);
            // 
            // bNula
            // 
            this.bNula.BackColor = System.Drawing.Color.MidnightBlue;
            this.bNula.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.bNula.Location = new System.Drawing.Point(77, 404);
            this.bNula.Name = "bNula";
            this.bNula.Size = new System.Drawing.Size(59, 56);
            this.bNula.TabIndex = 1;
            this.bNula.Text = "0";
            this.bNula.UseVisualStyleBackColor = false;
            this.bNula.Click += new System.EventHandler(this.bNula_Click);
            // 
            // bTocka
            // 
            this.bTocka.BackColor = System.Drawing.Color.LightSteelBlue;
            this.bTocka.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.bTocka.Location = new System.Drawing.Point(12, 404);
            this.bTocka.Name = "bTocka";
            this.bTocka.Size = new System.Drawing.Size(59, 56);
            this.bTocka.TabIndex = 2;
            this.bTocka.Text = ".";
            this.bTocka.UseVisualStyleBackColor = false;
            this.bTocka.Click += new System.EventHandler(this.bTocka_Click);
            // 
            // bJednako
            // 
            this.bJednako.BackColor = System.Drawing.Color.LightSteelBlue;
            this.bJednako.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.bJednako.Location = new System.Drawing.Point(142, 404);
            this.bJednako.Name = "bJednako";
            this.bJednako.Size = new System.Drawing.Size(59, 56);
            this.bJednako.TabIndex = 3;
            this.bJednako.Text = "=";
            this.bJednako.UseVisualStyleBackColor = false;
            this.bJednako.Click += new System.EventHandler(this.bJednako_Click);
            // 
            // bJedan
            // 
            this.bJedan.BackColor = System.Drawing.Color.MidnightBlue;
            this.bJedan.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.bJedan.Location = new System.Drawing.Point(12, 342);
            this.bJedan.Name = "bJedan";
            this.bJedan.Size = new System.Drawing.Size(59, 56);
            this.bJedan.TabIndex = 4;
            this.bJedan.Text = "1";
            this.bJedan.UseVisualStyleBackColor = false;
            this.bJedan.Click += new System.EventHandler(this.bJedan_Click);
            // 
            // bDva
            // 
            this.bDva.BackColor = System.Drawing.Color.MidnightBlue;
            this.bDva.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.bDva.Location = new System.Drawing.Point(77, 342);
            this.bDva.Name = "bDva";
            this.bDva.Size = new System.Drawing.Size(59, 56);
            this.bDva.TabIndex = 5;
            this.bDva.Text = "2";
            this.bDva.UseVisualStyleBackColor = false;
            this.bDva.Click += new System.EventHandler(this.bDva_Click);
            // 
            // bTri
            // 
            this.bTri.BackColor = System.Drawing.Color.MidnightBlue;
            this.bTri.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.bTri.Location = new System.Drawing.Point(142, 342);
            this.bTri.Name = "bTri";
            this.bTri.Size = new System.Drawing.Size(59, 56);
            this.bTri.TabIndex = 6;
            this.bTri.Text = "3";
            this.bTri.UseVisualStyleBackColor = false;
            this.bTri.Click += new System.EventHandler(this.bTri_Click);
            // 
            // bPlus
            // 
            this.bPlus.BackColor = System.Drawing.Color.LightSteelBlue;
            this.bPlus.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.bPlus.Location = new System.Drawing.Point(207, 404);
            this.bPlus.Name = "bPlus";
            this.bPlus.Size = new System.Drawing.Size(59, 56);
            this.bPlus.TabIndex = 7;
            this.bPlus.Text = "+";
            this.bPlus.UseVisualStyleBackColor = false;
            this.bPlus.Click += new System.EventHandler(this.bPlus_Click);
            // 
            // bCetiri
            // 
            this.bCetiri.BackColor = System.Drawing.Color.MidnightBlue;
            this.bCetiri.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.bCetiri.Location = new System.Drawing.Point(12, 280);
            this.bCetiri.Name = "bCetiri";
            this.bCetiri.Size = new System.Drawing.Size(59, 56);
            this.bCetiri.TabIndex = 8;
            this.bCetiri.Text = "4";
            this.bCetiri.UseVisualStyleBackColor = false;
            this.bCetiri.Click += new System.EventHandler(this.bCetiri_Click);
            // 
            // bPet
            // 
            this.bPet.BackColor = System.Drawing.Color.MidnightBlue;
            this.bPet.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.bPet.Location = new System.Drawing.Point(77, 280);
            this.bPet.Name = "bPet";
            this.bPet.Size = new System.Drawing.Size(59, 56);
            this.bPet.TabIndex = 9;
            this.bPet.Text = "5";
            this.bPet.UseVisualStyleBackColor = false;
            this.bPet.Click += new System.EventHandler(this.bPet_Click);
            // 
            // bSest
            // 
            this.bSest.BackColor = System.Drawing.Color.MidnightBlue;
            this.bSest.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.bSest.Location = new System.Drawing.Point(142, 280);
            this.bSest.Name = "bSest";
            this.bSest.Size = new System.Drawing.Size(59, 56);
            this.bSest.TabIndex = 10;
            this.bSest.Text = "6";
            this.bSest.UseVisualStyleBackColor = false;
            this.bSest.Click += new System.EventHandler(this.bSest_Click);
            // 
            // bMinus
            // 
            this.bMinus.BackColor = System.Drawing.Color.LightSteelBlue;
            this.bMinus.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.bMinus.Location = new System.Drawing.Point(207, 342);
            this.bMinus.Name = "bMinus";
            this.bMinus.Size = new System.Drawing.Size(59, 56);
            this.bMinus.TabIndex = 11;
            this.bMinus.Text = "-";
            this.bMinus.UseVisualStyleBackColor = false;
            this.bMinus.Click += new System.EventHandler(this.bMinus_Click);
            // 
            // bSedam
            // 
            this.bSedam.BackColor = System.Drawing.Color.MidnightBlue;
            this.bSedam.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.bSedam.Location = new System.Drawing.Point(12, 218);
            this.bSedam.Name = "bSedam";
            this.bSedam.Size = new System.Drawing.Size(59, 56);
            this.bSedam.TabIndex = 12;
            this.bSedam.Text = "7";
            this.bSedam.UseVisualStyleBackColor = false;
            this.bSedam.Click += new System.EventHandler(this.bSedam_Click);
            // 
            // bOsam
            // 
            this.bOsam.BackColor = System.Drawing.Color.MidnightBlue;
            this.bOsam.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.bOsam.Location = new System.Drawing.Point(77, 218);
            this.bOsam.Name = "bOsam";
            this.bOsam.Size = new System.Drawing.Size(59, 56);
            this.bOsam.TabIndex = 13;
            this.bOsam.Text = "8";
            this.bOsam.UseVisualStyleBackColor = false;
            this.bOsam.Click += new System.EventHandler(this.bOsam_Click);
            // 
            // bDevet
            // 
            this.bDevet.BackColor = System.Drawing.Color.MidnightBlue;
            this.bDevet.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.bDevet.Location = new System.Drawing.Point(142, 218);
            this.bDevet.Name = "bDevet";
            this.bDevet.Size = new System.Drawing.Size(59, 56);
            this.bDevet.TabIndex = 14;
            this.bDevet.Text = "9";
            this.bDevet.UseVisualStyleBackColor = false;
            this.bDevet.Click += new System.EventHandler(this.bDevet_Click);
            // 
            // bPuta
            // 
            this.bPuta.BackColor = System.Drawing.Color.LightSteelBlue;
            this.bPuta.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.bPuta.Location = new System.Drawing.Point(207, 280);
            this.bPuta.Name = "bPuta";
            this.bPuta.Size = new System.Drawing.Size(59, 56);
            this.bPuta.TabIndex = 15;
            this.bPuta.Text = "x";
            this.bPuta.UseVisualStyleBackColor = false;
            this.bPuta.Click += new System.EventHandler(this.bPuta_Click);
            // 
            // bKosinus
            // 
            this.bKosinus.BackColor = System.Drawing.Color.LightSteelBlue;
            this.bKosinus.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.bKosinus.Location = new System.Drawing.Point(12, 156);
            this.bKosinus.Name = "bKosinus";
            this.bKosinus.Size = new System.Drawing.Size(59, 56);
            this.bKosinus.TabIndex = 16;
            this.bKosinus.Text = "cos";
            this.bKosinus.UseVisualStyleBackColor = false;
            this.bKosinus.Click += new System.EventHandler(this.bKosinus_Click);
            // 
            // bKorijen
            // 
            this.bKorijen.BackColor = System.Drawing.Color.LightSteelBlue;
            this.bKorijen.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.bKorijen.Location = new System.Drawing.Point(77, 156);
            this.bKorijen.Name = "bKorijen";
            this.bKorijen.Size = new System.Drawing.Size(59, 56);
            this.bKorijen.TabIndex = 17;
            this.bKorijen.Text = "sqrt";
            this.bKorijen.UseVisualStyleBackColor = false;
            this.bKorijen.Click += new System.EventHandler(this.bKorijen_Click);
            // 
            // bKvadrat
            // 
            this.bKvadrat.BackColor = System.Drawing.Color.LightSteelBlue;
            this.bKvadrat.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.bKvadrat.Location = new System.Drawing.Point(142, 156);
            this.bKvadrat.Name = "bKvadrat";
            this.bKvadrat.Size = new System.Drawing.Size(59, 56);
            this.bKvadrat.TabIndex = 18;
            this.bKvadrat.Text = "pow";
            this.bKvadrat.UseVisualStyleBackColor = false;
            this.bKvadrat.Click += new System.EventHandler(this.bKvadrat_Click);
            // 
            // bPodijeljeno
            // 
            this.bPodijeljeno.BackColor = System.Drawing.Color.LightSteelBlue;
            this.bPodijeljeno.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.bPodijeljeno.Location = new System.Drawing.Point(207, 218);
            this.bPodijeljeno.Name = "bPodijeljeno";
            this.bPodijeljeno.Size = new System.Drawing.Size(59, 56);
            this.bPodijeljeno.TabIndex = 19;
            this.bPodijeljeno.Text = "/";
            this.bPodijeljeno.UseVisualStyleBackColor = false;
            this.bPodijeljeno.Click += new System.EventHandler(this.bPodijeljeno_Click);
            // 
            // bSinus
            // 
            this.bSinus.BackColor = System.Drawing.Color.LightSteelBlue;
            this.bSinus.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.bSinus.Location = new System.Drawing.Point(12, 94);
            this.bSinus.Name = "bSinus";
            this.bSinus.Size = new System.Drawing.Size(59, 56);
            this.bSinus.TabIndex = 20;
            this.bSinus.Text = "sin";
            this.bSinus.UseVisualStyleBackColor = false;
            this.bSinus.Click += new System.EventHandler(this.bSinus_Click);
            // 
            // bObrisiDio
            // 
            this.bObrisiDio.BackColor = System.Drawing.Color.LightSteelBlue;
            this.bObrisiDio.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.bObrisiDio.Location = new System.Drawing.Point(77, 94);
            this.bObrisiDio.Name = "bObrisiDio";
            this.bObrisiDio.Size = new System.Drawing.Size(59, 56);
            this.bObrisiDio.TabIndex = 21;
            this.bObrisiDio.Text = "CE";
            this.bObrisiDio.UseVisualStyleBackColor = false;
            this.bObrisiDio.Click += new System.EventHandler(this.bObrisiDio_Click);
            // 
            // bObrisiSve
            // 
            this.bObrisiSve.BackColor = System.Drawing.Color.LightSteelBlue;
            this.bObrisiSve.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.bObrisiSve.Location = new System.Drawing.Point(142, 94);
            this.bObrisiSve.Name = "bObrisiSve";
            this.bObrisiSve.Size = new System.Drawing.Size(59, 56);
            this.bObrisiSve.TabIndex = 22;
            this.bObrisiSve.Text = "C";
            this.bObrisiSve.UseVisualStyleBackColor = false;
            this.bObrisiSve.Click += new System.EventHandler(this.bObrisiSve_Click);
            // 
            // bDel
            // 
            this.bDel.BackColor = System.Drawing.Color.LightSteelBlue;
            this.bDel.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.bDel.Location = new System.Drawing.Point(207, 94);
            this.bDel.Name = "bDel";
            this.bDel.Size = new System.Drawing.Size(59, 56);
            this.bDel.TabIndex = 23;
            this.bDel.Text = "DEL";
            this.bDel.UseVisualStyleBackColor = false;
            this.bDel.Click += new System.EventHandler(this.bDel_Click);
            // 
            // txtPrikaz2
            // 
            this.txtPrikaz2.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.txtPrikaz2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtPrikaz2.Location = new System.Drawing.Point(12, 21);
            this.txtPrikaz2.Multiline = true;
            this.txtPrikaz2.Name = "txtPrikaz2";
            this.txtPrikaz2.Size = new System.Drawing.Size(254, 22);
            this.txtPrikaz2.TabIndex = 24;
            this.txtPrikaz2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtPrikaz1
            // 
            this.txtPrikaz1.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.txtPrikaz1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtPrikaz1.Location = new System.Drawing.Point(12, 49);
            this.txtPrikaz1.Multiline = true;
            this.txtPrikaz1.Name = "txtPrikaz1";
            this.txtPrikaz1.Size = new System.Drawing.Size(254, 22);
            this.txtPrikaz1.TabIndex = 25;
            this.txtPrikaz1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.ClientSize = new System.Drawing.Size(280, 472);
            this.Controls.Add(this.txtPrikaz1);
            this.Controls.Add(this.txtPrikaz2);
            this.Controls.Add(this.bDel);
            this.Controls.Add(this.bObrisiSve);
            this.Controls.Add(this.bObrisiDio);
            this.Controls.Add(this.bSinus);
            this.Controls.Add(this.bPodijeljeno);
            this.Controls.Add(this.bKvadrat);
            this.Controls.Add(this.bKorijen);
            this.Controls.Add(this.bKosinus);
            this.Controls.Add(this.bPuta);
            this.Controls.Add(this.bDevet);
            this.Controls.Add(this.bOsam);
            this.Controls.Add(this.bSedam);
            this.Controls.Add(this.bMinus);
            this.Controls.Add(this.bSest);
            this.Controls.Add(this.bPet);
            this.Controls.Add(this.bCetiri);
            this.Controls.Add(this.bPlus);
            this.Controls.Add(this.bTri);
            this.Controls.Add(this.bDva);
            this.Controls.Add(this.bJedan);
            this.Controls.Add(this.bJednako);
            this.Controls.Add(this.bTocka);
            this.Controls.Add(this.bNula);
            this.Controls.Add(this.bLogaritam);
            this.Name = "Form1";
            this.Text = "Kalkulator";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button bLogaritam;
        private System.Windows.Forms.Button bNula;
        private System.Windows.Forms.Button bTocka;
        private System.Windows.Forms.Button bJednako;
        private System.Windows.Forms.Button bJedan;
        private System.Windows.Forms.Button bDva;
        private System.Windows.Forms.Button bTri;
        private System.Windows.Forms.Button bPlus;
        private System.Windows.Forms.Button bCetiri;
        private System.Windows.Forms.Button bPet;
        private System.Windows.Forms.Button bSest;
        private System.Windows.Forms.Button bMinus;
        private System.Windows.Forms.Button bSedam;
        private System.Windows.Forms.Button bOsam;
        private System.Windows.Forms.Button bDevet;
        private System.Windows.Forms.Button bPuta;
        private System.Windows.Forms.Button bKosinus;
        private System.Windows.Forms.Button bKorijen;
        private System.Windows.Forms.Button bKvadrat;
        private System.Windows.Forms.Button bPodijeljeno;
        private System.Windows.Forms.Button bSinus;
        private System.Windows.Forms.Button bObrisiDio;
        private System.Windows.Forms.Button bObrisiSve;
        private System.Windows.Forms.Button bDel;
        private System.Windows.Forms.TextBox txtPrikaz2;
        private System.Windows.Forms.TextBox txtPrikaz1;
    }
}

