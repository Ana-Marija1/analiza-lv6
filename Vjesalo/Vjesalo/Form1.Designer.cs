﻿namespace Vjesalo
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.bGenerator = new System.Windows.Forms.Button();
            this.lBrojPokusaja = new System.Windows.Forms.Label();
            this.txtUnosSlova = new System.Windows.Forms.TextBox();
            this.bUnosSlova = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lBrojSlova = new System.Windows.Forms.Label();
            this.lBrojRijeci = new System.Windows.Forms.Label();
            this.lNepostojecaSlova = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // bGenerator
            // 
            this.bGenerator.Location = new System.Drawing.Point(243, 12);
            this.bGenerator.Name = "bGenerator";
            this.bGenerator.Size = new System.Drawing.Size(256, 38);
            this.bGenerator.TabIndex = 0;
            this.bGenerator.Text = "Generiraj ime države na engleskom";
            this.bGenerator.UseVisualStyleBackColor = true;
            this.bGenerator.Click += new System.EventHandler(this.bGenerator_Click);
            // 
            // lBrojPokusaja
            // 
            this.lBrojPokusaja.AutoSize = true;
            this.lBrojPokusaja.Location = new System.Drawing.Point(12, 127);
            this.lBrojPokusaja.Name = "lBrojPokusaja";
            this.lBrojPokusaja.Size = new System.Drawing.Size(0, 17);
            this.lBrojPokusaja.TabIndex = 3;
            // 
            // txtUnosSlova
            // 
            this.txtUnosSlova.Location = new System.Drawing.Point(132, 166);
            this.txtUnosSlova.MaxLength = 1;
            this.txtUnosSlova.Name = "txtUnosSlova";
            this.txtUnosSlova.Size = new System.Drawing.Size(65, 22);
            this.txtUnosSlova.TabIndex = 7;
            // 
            // bUnosSlova
            // 
            this.bUnosSlova.Location = new System.Drawing.Point(12, 162);
            this.bUnosSlova.Name = "bUnosSlova";
            this.bUnosSlova.Size = new System.Drawing.Size(110, 31);
            this.bUnosSlova.TabIndex = 9;
            this.bUnosSlova.Text = "Unos slova:";
            this.bUnosSlova.UseVisualStyleBackColor = true;
            this.bUnosSlova.Click += new System.EventHandler(this.bUnosSlova_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Location = new System.Drawing.Point(12, 56);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(770, 68);
            this.groupBox1.TabIndex = 11;
            this.groupBox1.TabStop = false;
            // 
            // lBrojSlova
            // 
            this.lBrojSlova.AutoSize = true;
            this.lBrojSlova.Location = new System.Drawing.Point(506, 127);
            this.lBrojSlova.Name = "lBrojSlova";
            this.lBrojSlova.Size = new System.Drawing.Size(0, 17);
            this.lBrojSlova.TabIndex = 12;
            // 
            // lBrojRijeci
            // 
            this.lBrojRijeci.AutoSize = true;
            this.lBrojRijeci.Location = new System.Drawing.Point(506, 144);
            this.lBrojRijeci.Name = "lBrojRijeci";
            this.lBrojRijeci.Size = new System.Drawing.Size(0, 17);
            this.lBrojRijeci.TabIndex = 13;
            // 
            // lNepostojecaSlova
            // 
            this.lNepostojecaSlova.AutoSize = true;
            this.lNepostojecaSlova.Location = new System.Drawing.Point(13, 144);
            this.lNepostojecaSlova.Name = "lNepostojecaSlova";
            this.lNepostojecaSlova.Size = new System.Drawing.Size(0, 17);
            this.lNepostojecaSlova.TabIndex = 14;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 208);
            this.Controls.Add(this.lNepostojecaSlova);
            this.Controls.Add(this.lBrojRijeci);
            this.Controls.Add(this.lBrojSlova);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.bUnosSlova);
            this.Controls.Add(this.txtUnosSlova);
            this.Controls.Add(this.lBrojPokusaja);
            this.Controls.Add(this.bGenerator);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button bGenerator;
        private System.Windows.Forms.Label lBrojPokusaja;
        private System.Windows.Forms.TextBox txtUnosSlova;
        private System.Windows.Forms.Button bUnosSlova;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lBrojSlova;
        private System.Windows.Forms.Label lBrojRijeci;
        private System.Windows.Forms.Label lNepostojecaSlova;
    }
}

