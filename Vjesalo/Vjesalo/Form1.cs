﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Vjesalo
{
    public partial class Form1 : Form
    {
        string word = "";
        List<Label> labels = new List<Label>();
        int numbTriesLeft = 8;
        int numbWord = 1;

        public Form1()
        {
            InitializeComponent();
        }

        static int zastavica = 0;

        string GenRandWord()
        {
            string filePath = @"D:/Imena_drzava.txt";
            string wordList = File.ReadAllText(filePath);
            string[] words = wordList.Split('\n');
            Random rand = new Random();
            return words[rand.Next(0, words.Length - 1)];
        }

        private void bGenerator_Click(object sender, EventArgs e)
        {
            if (zastavica == 0)
            {
                MakeMark();
                zastavica = 1;
            }
            
        }

        void MakeMark()
        {
            word = GenRandWord().ToUpper(); 
            //MessageBox.Show(word); //radi lakše provjere
            char[] letters = word.ToCharArray();
            for(int i = 0; i < letters.Length - 1; i++)
            {
                labels.Add(new Label());
                labels[i].Location = new Point((i * 15) + 5, 20);
                labels[i].Text = "_";
                labels[i].Parent = groupBox1;
                labels[i].BringToFront();
                labels[i].CreateControl();
                if(letters[i].ToString() == " ")
                {
                    numbWord ++;
                }
            }
            lBrojRijeci.Text = "Broj riječi: " + numbWord.ToString();
            lBrojSlova.Text = "Broj znakova: " + (letters.Length - 1).ToString();
            lBrojPokusaja.Text = "Broj pokušaja: " + numbTriesLeft;
        }

        private void bUnosSlova_Click(object sender, EventArgs e)
        {
            char letter = txtUnosSlova.Text.ToUpper().ToCharArray()[0];
            if (!char.IsLetter(letter))
            {
                MessageBox.Show("Nije unešeno slovo!", "Error", MessageBoxButtons.OK);
                return;
            }
            if (word.Contains(letter))
            {
                char[] letters = word.ToCharArray();
                for(int i = 0; i < letters.Length - 1; i++)
                {
                    if (letters[i] == letter)
                    {
                        labels[i].Text = letter.ToString();
                    }
                }
                txtUnosSlova.ResetText();
                int flag = 0;
                foreach (Label l in labels)
                    if (l.Text == "_") flag++;
                foreach (Label l in labels)
                {
                    if (numbWord < flag+1) return;
                }
                MessageBox.Show("Pobjeda!");
                ResetGame();
            }
            else
            {
                MessageBox.Show("Slovo se ne nalazi u pojmu.");
                lNepostojecaSlova.Text += " " + letter.ToString() + ",";
                numbTriesLeft--;
                if (numbTriesLeft == 0)
                {
                    MessageBox.Show("Poraz! Traženi pojam je " + word);
                    ResetGame();
                }
                lBrojPokusaja.Text = "Broj pokušaja: " + numbTriesLeft;
            }
            txtUnosSlova.ResetText();
        }
        void ResetGame()
        {
            groupBox1.ResetText();
            GenRandWord();
            
            numbTriesLeft = 8;
            numbWord = 1;
            lNepostojecaSlova.ResetText();

            MakeMark();
        }
    }
}